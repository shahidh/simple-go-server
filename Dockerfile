FROM golang:1.10.3-alpine3.7

# setup the working directory
WORKDIR /go/src/app

# add source code
ADD main.go main.go

# build the source
RUN go build main.go

# command to be executed on running the container
CMD ["./main"]