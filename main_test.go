package main

import "testing"

func TestConcatStrings(t *testing.T) {
	tt := []struct {
        name string
		in1 string
        in2 string
        out string
	}{
		{"normal", "a", "b", "ab"},
        {"numbers", "1", "2", "12"},
        {"empty", "", "", ""},
        {"one-empty", "", "aaa", "aaa"},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			got := concatStrings(tc.in1, tc.in2)
			if got != tc.out {
				t.Fatalf("expected '%d', got '%d'", tc.out, got)
			}
		})
	}
}